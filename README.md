# README #

This repo is for ToDo List Application. A simple single page application with a to-do list component that allows visitors to manage a list of tasks.

## API ##
The application depends on an api that is build on node, express, knex and bookshelf.
The API server should run on port 3000, http//localhost:3000.
Go to the api directory and run "npm run start", it uses nodemon, so it will automatically restart if any file in the api changes.
You should always run the api before the ui.

## SQL ##
The API is backed by a Postgre SQL sever on AWS. You should have an internet connection to run the api. All environments are configured to use the AWS database.
There is no sql implimented, the application uses a code-first approach so the tables are generated with migrations using knex.

## UI ##
The user interface is developed with React.js. Bootstrap and fontawesome used for styling.
The API cors allows a client on port 3001 on the same server, so you should run the app on on http//localhost:3001.
Go to the ui directory and run "npm start".
Port 3000 should be already taken by the api, verify that the ui runs on port 3001.


For more information, contact Aubrey Pule Khoza:
+27719471569
aubreypk@gmail.com
