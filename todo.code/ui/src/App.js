/*
  App runs the main react application
*/

import React, { Component } from 'react';
import './App.css';

// base url to API
const apiUrl = 'http://localhost:3000/tasks';
// TODO: repeating api calls in components
//  optimise component by creating an apiCallCenter function in external js file that will be called with parameters and handle all API calls
//  code in this file should be halfed

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { items: [], text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleIsDone = this.handleIsDone.bind(this);
    this.updateList = this.updateList.bind(this);
  }

  // renderes the component, this is a simple component so the are no child components
  render() {
    return (
      <div>
        <h3>Todo List App</h3>
        <form id="taskForm" onSubmit={this.handleSubmit}>
          <div class="form-group">
            <input type="text" class="form-control" id="inputTask" aria-describedby="taskHelp" placeholder="Add something to the list"
              onChange={this.handleChange}
              value={this.state.text}
            />
          </div>
          <button hidden>
          Add #{this.state.items.length + 1}
          </button>
        </form>
        <ul class="list-group">
          {this.state.items.map(item => (
            <li class="list-group-item d-flex justify-content-between align-items-center" key={item.id}>
              {item.is_done ? (
                <button type="button" class="btn btn-light" onClick={this.handleIsDone.bind(this, item.id)}><span class="badge badge-light"><i class="fa fa-check fa-2x text-success"></i></span></button>
              ) : (
                <button type="button" class="btn btn-light" onClick={this.handleIsDone.bind(this, item.id)}><span class="badge badge-light"><i class="fa fa-check fa-2x"></i></span></button>
              )}
              {item.description}
              <button type="button" class="btn btn-light" onClick={this.handleDelete.bind(this, item.id)}><span class="badge badge-light badge-pill"><i class="fa fa-times fa-2x"></i></span></button>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  // Update list used to call the api and update the items in state
  updateList(e){
    fetch(apiUrl) 
    .then(response => {
      return response.json();
    })
    .then(json => {
      this.setState({items:json, text: ''});
    })
    .catch(error => {
      console.log(error);
    });
  }

  // call the api the first time, use this method to make sure that the component is already active before fetching
  componentDidMount(){
    fetch(apiUrl) 
    .then(response => {
      return response.json();
    })
    .then(json => {
      this.setState({items:json, text: ''});
    })
    .catch(error => {
      console.log(error);
    });
  }

  // on change of input box, update text instate
  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  // on submit, use the api to post
  handleSubmit(e) {
    e.preventDefault();
    // return if no text entered
    if (!this.state.text.length) {
      return;
    }
    // create an ovject to be used
    const newItem = {
      description: this.state.text,
      is_done: false
    };
    // convert the object to searchParams. 
    // x-www-form-urlencoded works best with SearchParams
    const searchParams = Object.keys(newItem).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(newItem[key]);
    }).join('&');

    // call api wiht fetch and handle results, log errors
    fetch(apiUrl, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: searchParams
    })
    .then(response => {
      return response.json();
    })
    .then(json => {
      this.setState(prevState => ({
        items: prevState.items.concat(json),
        text: ''
      }));
    })
    .catch(error => {
      console.log(error);
    });
  }

  // When the delete button is clicked, use delete on api to delete item with id
  handleDelete(id, e){
    e.preventDefault();
    fetch(apiUrl + "/" + id, {
      method: 'delete'
    })
    .then(result => {
      this.updateList(this);
    })
    .catch(error => {
      console.log(error);
    });
  }

  // when is done button is clicked, set isdone by using update from api
  // same logic as post
  handleIsDone(id, e){
    e.preventDefault();

    const updatedItem = {
      is_done: true
    };

    const searchParams = Object.keys(updatedItem).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(updatedItem[key]);
    }).join('&');

    fetch(apiUrl + "/" + id, {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: searchParams
    })
    .then(result => {
      this.updateList(this);
    })
    .catch(error => {
      console.log(error);
    });
  }

}

export default App;
