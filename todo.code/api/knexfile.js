// Update with your config settings.

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      host: 'test.ckyfh63famfu.eu-west-2.rds.amazonaws.com',
      database: 'test',
      user:     'developer',
      password: 'd3v3l0p3rsch3m4s321'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'khoza.knex_migrations'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      host: 'test.ckyfh63famfu.eu-west-2.rds.amazonaws.com',
      database: 'test',
      user:     'developer',
      password: 'd3v3l0p3rsch3m4s321'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'khoza.knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      host: 'test.ckyfh63famfu.eu-west-2.rds.amazonaws.com',
      database: 'test',
      user:     'developer',
      password: 'd3v3l0p3rsch3m4s321'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'khoza.knex_migrations'
    }
  }
};
