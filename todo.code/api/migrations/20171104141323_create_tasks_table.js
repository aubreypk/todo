
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('khoza.tasks', function(table) {
          table.increments();
          table.string('description').unique();
          table.boolean('is_done');
          table.timestamps();
        })
      ]);
};

exports.down = function(knex, Promise) {
  
};
