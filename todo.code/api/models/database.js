/* 
  This file handles database connectivity 
  It is based on Knex and Bookshelf
*/

// knex object to be used by bookshelf
// for different environment setings and migration settings see knexfile.js
var knex = require('knex')({
    client: 'pg',
    connection: {
      host     : 'test.ckyfh63famfu.eu-west-2.rds.amazonaws.com',
      user     : 'developer',
      password : 'd3v3l0p3rsch3m4s321',
      database : 'test',
      charset  : 'utf8'
    }
});
  
// bookshel instance to be exported and used by models
var bookshelf = require('bookshelf')(knex);
bookshelf.plugin('registry');

module.exports = bookshelf;
