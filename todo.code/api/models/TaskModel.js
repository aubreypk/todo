/* Task model  */

// use bookshelf, only the first model instantiated will create an instance
var bookshelf = require('./database');

// connects to schema.table = khoza.tasks
var Task = bookshelf.Model.extend({
    tableName: 'khoza.tasks',
    hasTimestamps: true
});

// export using bookshel.model
module.exports = bookshelf.model('Task', Task);
