/* 
    Routes for todo methods 
*/

'use strict';

// use express library for routing
var express = require('express');
var todoRoutes = express.Router();

// requre the controller -> object
var todo = require('./../controllers/todoController');

// middleware: just to log the request time
todoRoutes.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// define the listing routes
todoRoutes.get('/', todo.listTasks);
todoRoutes.post('/', todo.createTask);
// define the listing routes
todoRoutes.get('/:taskId', todo.readTask);
todoRoutes.put('/:taskId', todo.updateTask);
todoRoutes.delete('/:taskId', todo.deleteTask);

// export this routes to be used in main server
module.exports = todoRoutes;
