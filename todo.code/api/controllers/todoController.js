/*
  Controller for Todo
  Impliments CRUD methods for the Task model
  Methods expect requests as the default x-www-form-urlencoded
  Id is always passed as param (url/id)
  All results are returned in JSON
*/

'use strict';

// GET: Use the task model
var Task = require('./../models/TaskModel');

// Returns a listing of all taks
var listTasks = function(request, result) {
    new Task().fetchAll()
        .then(function (tasks) {
            result.json(tasks);
        }).catch(function (error) {
            result.json(error);
    });
};

// POST: creates a new task and saves it in DB
var createTask = function(request, result) {
    var newTask = new Task(request.body);
    newTask.save()
    .then(function(task) {
      result.json(task);
    })
    .catch(function(error){
      result.json(error);
    });
};

// GET: return a task by its Id
var readTask = function(request, result) {
  var taskId = request.params.taskId;
  new Task().where('id', taskId)
    .fetch()
    .then(function(task){
      result.json(task);
    })
    .catch(function(error){
      result.json(error);
  });
};

// UPDATE: update a task with teh requeest
var updateTask = function(request, result) {
  // Bookshelf will only update given fields, other fields ignored :-)
  var taskId = request.params.taskId;
  new Task({'id': taskId})
    .save(request.body)
    .then(function(task){
      result.json(task);
    })
    .catch(function(error){
      result.json(error);
  });
};

// DELETE: delete a task with teh given id
// TODO: update model to have is_deleted field and set that flag instead of deleting from db
//  deleting from db might not be a good practice in some cases
var deleteTask = function(request, result) {
  var taskId = request.params.taskId;
  new Task().where('id', taskId)
    .destroy()
    .then(function(){
      result.send('Successfully deleted task ' + taskId);
    })
    .catch(function(error){
      result.json(err);
  });
};

// export the method and make them availabe for usage
module.exports = {
	listTasks: listTasks,
  createTask: createTask,
  readTask: readTask,
  updateTask: updateTask,
  deleteTask: deleteTask
};
