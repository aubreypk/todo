/*
    Main server file
    Everything happens from here
*/

// USe express, run on port 3000 (process.env.PORT is not set)
var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

// use body-parser to parse requests
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// Default route
app.get('/', function (req, res) {
    res.send('ToDo List API v1.0.0')
});
// include route files
var todoRoute = require('./routes/todoRoutes');
app.use('/tasks', todoRoute);

// middleware
app.use(function (req, res, next) {
    // had CORS issues :-(
    // this will allow client on 3001 on same server to perform all methods
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions) - !no used
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// Listen
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);
